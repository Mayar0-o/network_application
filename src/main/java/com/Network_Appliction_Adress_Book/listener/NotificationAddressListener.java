package com.Network_Appliction_Adress_Book.listener;

import javax.management.NotificationListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class NotificationAddressListener extends JobExecutionListenerSupport {
	
	   private static final Logger LOGGER = LoggerFactory.getLogger(NotificationListener.class);

	    
	 @Override
	    public void afterJob(final JobExecution jobExecution) {
	        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
	            LOGGER.info("!!! JOB FINISHED! Time to verify the results");

	        }
	    }
	

}
