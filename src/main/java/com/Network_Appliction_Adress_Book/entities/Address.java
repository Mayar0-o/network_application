package com.Network_Appliction_Adress_Book.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.validation.constraints.NotBlank;

import javax.persistence.Version;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.Network_Appliction_Adress_Book.Aspect.Auditable;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "Contact")
@Audited
public class Address extends Auditable<String> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@NotBlank(message = "First Name is mandatory")
	private String firstname;
	private String lastname;
	@NotNull
	@Min(value = 1, message = "Age is invalid")
	private Integer age;
	@NotBlank(message = "Address is mandatory")
	private String address;
	// @ColumnDefault("0")
	
	private Boolean valid = false;
	
	
	@Min(value = 9, message = "Phone1 is invalid")
	private Integer phone1;
	
	@Min(value = 9, message = "Phone2 is invalid")
	private Integer phone2;
	
	//@JsonIgnore
	@ManyToOne
	@JoinColumn(insertable = true, updatable = true, nullable = false)
	private User user;


	/*@OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
	private List<PhoneNumber> phonenumber;*/
	
	// will add implicit LockModeType.OPTIMISTIC_FORCE_INCREMENT
	@JsonIgnore
	@Version
	private Integer version = 0;

	public Address() {
	}

	public Address(Integer id, String firstname, String lastname, Integer age, String address) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getPhone1() {
		return phone1;
	}

	public void setPhone1(Integer phone1) {
		this.phone1 = phone1;
	}

	public Integer getPhone2() {
		return phone2;
	}

	public void setPhone2(Integer phone2) {
		this.phone2 = phone2;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}


	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", age=" + age
				+ ", address=" + address + "]";
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

}
