package com.Network_Appliction_Adress_Book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.transaction.annotation.EnableTransactionManagement;


//add attribute for display default login for Security
@EnableTransactionManagement
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class, 
                
})


//@EnableRedisHttpSession
public class NetworkApplictionAdressBookApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(NetworkApplictionAdressBookApplication.class, args);
	}

}
