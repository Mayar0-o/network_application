package com.Network_Appliction_Adress_Book.service;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService, ICrudGenericService<User, Integer> {

	@Autowired
	private UserRepository userRepository;

	// find user and give role
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(userName);
		try {
			if (user == null) {
				throw new UsernameNotFoundException("No user found with username: " + userName);
			}
			return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
					getAuthorities(user));
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	

	@Override
	public List<User> getAll() {
		return userRepository.findAll();
	}

	private static Collection<? extends GrantedAuthority> getAuthorities(User user) {
		String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
		Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
		return authorities;
	}

	@Override
	public boolean add(User entity) {
		return false;
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Integer id, User entity) {
		User userDes = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User id" + id));
		userRepository.save(userDes);
	}

	@Override
	public boolean delete(Integer id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		userRepository.delete(user);
		return true;
	}

	@Override
	public User get(Integer id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		return user;
	}

}
