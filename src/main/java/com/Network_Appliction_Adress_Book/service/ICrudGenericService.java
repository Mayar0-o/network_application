package com.Network_Appliction_Adress_Book.service;

import java.util.List;

public interface ICrudGenericService<T, ID> {

	List<T> getAll();

	boolean add(T entity);

	void update(ID id, T entity);

	boolean delete(ID id);
	
	T get(ID id);
}
