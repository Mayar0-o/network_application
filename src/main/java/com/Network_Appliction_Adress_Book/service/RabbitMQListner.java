package com.Network_Appliction_Adress_Book.service;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.messaging.handler.annotation.support.MessageHandlerMethodFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.repositories.AddressRepository;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.Network_Appliction_Adress_Book.config.*;

@Component
public class RabbitMQListner {

	@Autowired
	private AddressService addressService;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private ObjectMapper mapper;

	static final Logger logger = LoggerFactory.getLogger(RabbitMQListner.class);

	@Value("${My.rabbitmq.queue}")
	public String queueName;
	
	 @PersistenceContext
     private EntityManager entityManager;

	@RabbitListener(queues = "${My.rabbitmq.queue}")
	public void processOrder(Address address) {
		// Address addressDes = new Address();
		// addressDes = mapper.convertValue(address, Address.class);		
		try {
			this.save(address);			
		} catch (Exception e) {
			logger.info("Error Received: " + e.getMessage());
		}
		// logger.info("Contact Received: "+address);
		// 
		logger.info("Contact Received: " + address );

	}
	
	//@Transactional
    public Boolean save(Address address) {		
		return addressService.AddJs(address);         
    }
}
