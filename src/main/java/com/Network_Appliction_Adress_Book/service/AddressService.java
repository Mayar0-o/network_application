package com.Network_Appliction_Adress_Book.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
//import javax.transaction.Transactional;
import javax.persistence.PersistenceContext;
/*
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.*;
import com.Network_Appliction_Adress_Book.repositories.AddressRepository;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.Network_Appliction_Adress_Book.entities.*;

@Service
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE)
public class AddressService implements ICrudGenericService<Address, Integer> {
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private UserRepository userRepository;

	/*
	 * @Autowired private RuntimeService runtimeService;
	 * 
	 * @Autowired private TaskService taskService;
	 * 
	 * @Autowired private RepositoryService repositoryService;
	 */

	// for Optims lock
	/* */

	@Autowired
	private UserDetailsService userService;

	@PersistenceContext
	private EntityManager entityManager;

	@Cacheable
	public List<Address> GetAddressListforUser() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication auth = securityContext.getAuthentication();
		try {
			User user = userRepository.findByEmail(auth.getName());
			return addressRepository.findByUser(user);
		} catch (Exception e) {
		}

		return null;
	}

	@Override
	public List<Address> getAll() {
		return addressRepository.findAll();
	}

	public List<Address> getByUserBranch() {
		List<Address> Temp = new ArrayList<Address>();
		SecurityContext s = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication authentication = s.getAuthentication();
		Branch userBranch = userRepository.findByEmail(authentication.getName()).getBranch();
		List<User> userList = userRepository.findByBranch(userBranch);
		for (User user2 : userList) {
			for (Address address : user2.getAddress()) {
				Temp.add(address);
			}
		}
		return Temp;
	}

	public void SetValid(Integer id) {
		Address address = addressRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid Address id" + id));
		address.setValid(true);
		addressRepository.save(address);
	}

	public List<Address> GetAddressByValid(Boolean valid) {
		return addressRepository.findByvalid(valid);
	}

	@Override
	public boolean delete(Integer id) {

		SecurityContext securityContext = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication auth = securityContext.getAuthentication();
		boolean hasAdminRole = auth.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

		try {
			User user = userRepository.findByEmail(auth.getName());

			Optional<Address> address = addressRepository.findById(id);
			if ((user.getId()) == (address.get().getUser().getId()) || (hasAdminRole)) {

				addressRepository.deleteById(id);
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	@Override
	public boolean add(Address address) {
		SecurityContext s = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication authentication = s.getAuthentication();
		boolean hasUserRole = authentication.getAuthorities().stream()
				.anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
		User user = userRepository.findByEmail(authentication.getName());
		address.setValid(hasUserRole);
		address.setUser(user);
		addressRepository.save(address);

		Map<String, Object> variables = new HashMap<>();
		variables.put("valid", hasUserRole);
		return hasUserRole;

		// runtimeService.startProcessInstanceByKey("Add-Contact", variables);

	}

	/*
	 * // fetch task assigned public List<Task> getTasks(String assignee) { return
	 * taskService.createTaskQuery().taskAssignee(assignee).list(); }
	 * 
	 * // complete the task public void completeTask(String taskId) {
	 * taskService.complete(taskId); }
	 * 
	 * // fetching process and task information public String processInformation() {
	 * 
	 * List<Task> taskList =
	 * taskService.createTaskQuery().orderByTaskCreateTime().asc().list();
	 * 
	 * StringBuilder processAndTaskInfo = new StringBuilder();
	 * 
	 * processAndTaskInfo.append("Number of process definition available: " +
	 * repositoryService.createProcessDefinitionQuery().count() +
	 * " | Task Details= ");
	 * 
	 * taskList.forEach(task -> {
	 * 
	 * processAndTaskInfo.append("ID: " + task.getId() + ", Name: " + task.getName()
	 * + ", Assignee: " + task.getAssignee() + ", Description: " +
	 * task.getDescription()); });
	 * 
	 * return processAndTaskInfo.toString(); }
	 */

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean AddJs(Address address) {
		try {
			Optional<User> user = userRepository.findById(address.getUser().getId());
			UserDetails userDetails = userService.loadUserByUsername(user.get().getEmail());
			Authentication auth = new UsernamePasswordAuthenticationToken(userDetails.getUsername(),
					userDetails.getPassword(), userDetails.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(auth);
			boolean AdminRole = auth.getAuthorities().stream()
					.anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
			address.setUser(user.get());
			address.setValid(AdminRole);
			addressRepository.saveAndFlush(address);
			return true;
		} catch (Exception e) {

			return false;
		}

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public void update(Integer id, Address addressSrc) {
		/*
		 * User user = entityManager.find(User.class, id);
		 * 
		 * entityManager.lock(user, LockModeType.OPTIMISTIC);
		 */
		/*
		 * Address address = entityManager.find( Address.class, 1L );
		 */

		Address address = entityManager.find(Address.class, id);
		// product.setQuantity(0);

		entityManager.lock(address, LockModeType.PESSIMISTIC_WRITE);

		/*
		 * entityManager.lock(address, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
		 */

		/*
		 * Address address = addressRepository.findById(id) .orElseThrow(() -> new
		 * IllegalArgumentException("Invalid Address id" + id));
		 */

		address.setAddress(addressSrc.getAddress());
		address.setAge(addressSrc.getAge());
		address.setFirstname(addressSrc.getFirstname());
		address.setLastname(addressSrc.getLastname());
		address.setPhone1(addressSrc.getPhone1());
		address.setPhone2(addressSrc.getPhone2());
		addressRepository.save(address);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void Merge(Integer DesID, Integer SrcID, Integer op) {
		Address addressDes = entityManager.find(Address.class, DesID, LockModeType.PESSIMISTIC_WRITE);
		Address addressSrc = entityManager.find(Address.class, SrcID, LockModeType.PESSIMISTIC_WRITE);
		switch (op) {
		case 1:
			addressDes.setPhone2(addressSrc.getPhone1());
			addressRepository.save(addressDes);
			addressRepository.delete(addressSrc);
			break;

		case 2:
			org.springframework.beans.BeanUtils.copyProperties(addressSrc, addressDes);
			addressRepository.delete(addressSrc);
			break;

		default:
			break;

		}

	}

	@Override
	public Address get(Integer id) {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication auth = securityContext.getAuthentication();
		boolean hasAdminRole = auth.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));
		try {
			User user = userRepository.findByEmail(auth.getName());
			Optional<Address> address = addressRepository.findById(id);
			boolean b = address.get().getUser().getBranch().equals(user.getBranch());
			if (((user.getId()).equals(address.get().getUser().getId())) || (hasAdminRole) || b) {
				return address.get();

			}
		} catch (Exception e) {
		}
		return null;

	}

}
