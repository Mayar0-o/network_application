package com.Network_Appliction_Adress_Book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.Network_Appliction_Adress_Book.entities.Branch;

public interface BranchRepository extends JpaRepository<Branch, Integer>{

}
