package com.Network_Appliction_Adress_Book.repositories;

import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.Branch;
import com.Network_Appliction_Adress_Book.entities.User;

public interface AddressRepository extends JpaRepository<Address, Integer> {

	// @Query("Select * from Address where BranchID = :BranchID")
	// List<Address> findAddressByBranch(Integer BranchID);

	// Optional<Address> findById(Integer id);
	
	Optional<Address> findById(Integer id);
	
	Address findByfirstname(String firstname);
	// @Lock(LockModeType.PESSIMISTIC_WRITE)
	List<Address> findByUser(User user);
		
	List<Address> findByvalid(Boolean valid);

	/*
	 * @Async
	 * 
	 * @Modifying
	 * 
	 * @Transactional(rollbackFor=Exception.class)
	 */
	
	@Query(nativeQuery = true, value = "SELECT ad.id, ad.address, ad.age, ad.age,  ad.firstname, ad.lastname, ad.valid, ad.user_id, ad.version, ad.phone1, ad.phone2  from users as us INNER JOIN contact as ad on us.id= ad.user_id WHERE us.branch_id= :bra and ad.valid = 1")
	List<Address> findByBanch(@Param("bra") Integer branch);

}
