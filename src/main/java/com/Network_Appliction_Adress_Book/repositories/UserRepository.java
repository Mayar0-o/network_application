package com.Network_Appliction_Adress_Book.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Network_Appliction_Adress_Book.entities.Branch;
import com.Network_Appliction_Adress_Book.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByName(String email);

	User findByEmail(String email);
	
	List<User> findByBranch(Branch branch);

	// User findByEmail(String email);

}
