package com.Network_Appliction_Adress_Book.config;

import javax.sql.DataSource;

import org.apache.tomcat.util.file.ConfigurationSource.Resource;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.processor.AddressProcessor;
import com.Network_Appliction_Adress_Book.config.*;
import org.springframework.core.io.FileSystemResource;



@Configuration
@EnableBatchProcessing
public class BatchConfig {
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Autowired
	private DataSource dataSource;
		 
	
	@Bean
	public JdbcCursorItemReader<Address> reader(){
		System.out.println("reader Run...........");
		JdbcCursorItemReader<Address> cursorItemReader = new JdbcCursorItemReader<>();
		cursorItemReader.setDataSource(dataSource);
		cursorItemReader.setSql("SELECT CO.id, CO.firstname, CO.lastname, CO.address, CO.age, Co.phone1, Co.phone2 FROM contact AS CO");
		cursorItemReader.setRowMapper(new AddressRowMapper());
		return cursorItemReader;
	}
	
	@Bean
	public AddressProcessor processor(){
		return new AddressProcessor();
	}
	
	@Bean
	public FlatFileItemWriter<Address> writer(){
		System.out.println("write Run...........");
		FlatFileItemWriter<Address> writer = new FlatFileItemWriter<Address>();
		writer.setResource(new FileSystemResource("resources/outputData.csv"));		
		
		DelimitedLineAggregator<Address> lineAggregator = new DelimitedLineAggregator<Address>();
		lineAggregator.setDelimiter(",");
		
		BeanWrapperFieldExtractor<Address>  fieldExtractor = new BeanWrapperFieldExtractor<Address>();
		fieldExtractor.setNames(new String[]{"id","firstname", "lastname", "address", "phone1", "phone2", "age"});
		
		lineAggregator.setFieldExtractor(fieldExtractor);
		
		writer.setLineAggregator(lineAggregator);
				
		return writer;
	}
	
	@Bean
	public Step step1(){
		System.out.println("Run Step 1...........");
		return stepBuilderFactory.get("step1")
				.<Address,Address>chunk(20)
				.reader(reader())
				.processor(processor())
				.writer(writer())
				.build();
	}

	@Bean
	public Job exportAddressJob(){
		System.out.println("exportAddressJob  ...........");
		return jobBuilderFactory.get("exportAddressJob")
				.incrementer(new RunIdIncrementer())
				.flow(step1())
				.end()
				.build();
	}
	}
