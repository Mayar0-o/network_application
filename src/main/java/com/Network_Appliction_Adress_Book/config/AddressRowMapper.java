package com.Network_Appliction_Adress_Book.config;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.Network_Appliction_Adress_Book.entities.Address;


public class AddressRowMapper implements RowMapper<Address> {

	@Override
	public Address mapRow(ResultSet rs, int rowNum) throws SQLException {
		Address address = new Address();
		address.setId(rs.getInt("id"));
		address.setFirstname(rs.getString("firstname"));
		address.setLastname(rs.getString("lastname"));
		address.setAddress(rs.getString("address"));
		address.setAge(rs.getInt("age"));
		address.setPhone1(rs.getInt("phone1"));
		address.setPhone2(rs.getInt("phone2"));
		System.out.print("Run...........");
		return address;
	}

}
