package com.Network_Appliction_Adress_Book.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.messaging.handler.annotation.support.MessageHandlerMethodFactory;
import org.springframework.stereotype.Service;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.service.RabbitMQListner;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.x.protobuf.MysqlxResultset.ContentType_BYTES;
import com.rabbitmq.client.MessageProperties;



@Configuration
public class RabbitMQConfig  implements RabbitListenerConfigurer {
	
	@Value("${My.rabbitmq.queue}")
	public  String queueName;

	@Value("${spring.rabbitmq.username}")
	String username;

	@Value("${spring.rabbitmq.password}")
	private String password;

	@Value("${My.rabbitmq.exchange}")
    public  String EXCHANGEName;

	// for Send message
	/*@Autowired
	private ObjectMapper objectMapper;
	 
	public void sendOrder(Address address) {
	  try {
	        String orderJson = objectMapper.writeValueAsString(address);
	        Message message = MessageBuilder
	                            .withBody(orderJson.getBytes())
	                            .setContentType(MessageProperties.CONTENT_TYPE_JSON)
	                            .build();
	        this.rabbitTemplate.convertAndSend(queueName, message);
	    } catch (JsonProcessingException e) {
	        e.printStackTrace();
	    }
	}
    */
	
	@Bean
    ObjectMapper mapper(){		
		 return new ObjectMapper();	
	}

    @Bean
    Exchange ordersExchange() {
    	//ExchangeBuilder.fanoutExchange(EXCHANGEName).build();
    	
        return ExchangeBuilder.topicExchange(EXCHANGEName).build();
    }
    
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
    @Bean
    Binding binding(Queue ordersQueue, TopicExchange ordersExchange) {
        return BindingBuilder.bind(ordersQueue).to(ordersExchange).with(queueName);
    }

	@Bean
	Queue queue() {
		return new Queue(queueName, false);
	}
	//create MessageListenerContainer using default connection factory
	
		@Override
	    public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
	        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
	    }
		
		
	    @Bean
	    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
	       final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
	       rabbitTemplate.addAfterReceivePostProcessors(m -> {
	    	   			m.getMessageProperties().setContentType(ContentType_BYTES.JSON.toString());
	    	   		return m;
	    	   		}
	       );
	        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
	        return rabbitTemplate;
	    }
	    

	    @Bean
	    MessageHandlerMethodFactory messageHandlerMethodFactory() {
	        DefaultMessageHandlerMethodFactory messageHandlerMethodFactory = new DefaultMessageHandlerMethodFactory();
	        messageHandlerMethodFactory.setMessageConverter(consumerJackson2MessageConverter());
	        return messageHandlerMethodFactory;
	    }
	 
	    @Bean
	    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
	        return new MappingJackson2MessageConverter();
	    }

	
		

	
}
