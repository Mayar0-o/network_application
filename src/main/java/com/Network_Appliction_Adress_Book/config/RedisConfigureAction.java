package com.Network_Appliction_Adress_Book.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.ConfigureRedisAction;
//@Configuration
public class RedisConfigureAction {
    @Bean
    public ConfigureRedisAction configureRedisAction() {
        return ConfigureRedisAction.NO_OP;
    }
    
    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
            RedisStandaloneConfiguration redisConf = new RedisStandaloneConfiguration();
            redisConf.setHostName("localhost");
            redisConf.setPort(6379);            

            LettuceConnectionFactory factory = new LettuceConnectionFactory(redisConf);
            factory.setTimeout(500L); //timeout to redis

            return factory;
        }
}