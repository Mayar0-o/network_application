package com.Network_Appliction_Adress_Book.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.repositories.AddressRepository;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.Network_Appliction_Adress_Book.service.AddressService;

@RestController
@RequestMapping("/api/v1")
public class RestApiContorller {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private AddressService addressService;
	
	
	
    @RequestMapping("/getSeesion")
    public String getSeesion(HttpServletRequest request) {
        String sessionId = request.getSession().getId();
        int serverPort = request.getServerPort();
        System.out.println("Current request session:" + sessionId);
        return "Request server port number:" + serverPort + ",Current server's seionId:" + sessionId;
    }
    
	@RequestMapping(value ="/address/{id}",  method = RequestMethod.GET,  produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<Address> getUsersById(@PathVariable(value = "id") int Id) {
		Address address = addressService.get(Id);
		return ResponseEntity.ok().body(address);
	}
	
	 	 	 
	
	@RequestMapping(value="/update/address/{id}", method= RequestMethod.PUT,produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<Address> updateUser(@PathVariable(value = "id") int addressId,
			@Valid @RequestBody Address addressDetails) {
		Address address = addressRepository.findById(addressId)
				.orElseThrow(() -> new IllegalArgumentException("User not found on :: " + addressId));
		address.setAddress(addressDetails.getAddress());
		address.setLastname(addressDetails.getLastname());
		address.setFirstname(addressDetails.getFirstname());
		final Address updatedAddress = addressRepository.save(address);
		return ResponseEntity.ok(updatedAddress);
	}

	@PostMapping("/address/add")
	public Map<String, Boolean>  createAddress(@Valid @RequestBody Address address) {
		Map<String, Boolean> response = new HashMap<>();
		response.put("valid", addressService.add(address));
		return response;				
	}
	
	/*@RequestMapping(value ="/address/delete/{id}", produces={"application/xml", "application/json"})
    @ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") int id) {
		Map<String, Boolean> response = new HashMap<>();
		if (addressService.delete(id)) {			
			response.put("deleted", Boolean.TRUE);				
		}
		else {
			response.put("deleted", Boolean.FALSE);
		}
		return response;
	    }

	@RequestMapping(value= "/address/delete/{id}")
	public String deleteUser(@PathVariable(value = "id") int addressId,Model model) throws Exception {	
		return "redirect:/home";
	}*/

}
