package com.Network_Appliction_Adress_Book.web;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.repositories.AddressRepository;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.Network_Appliction_Adress_Book.service.AddressService;
import com.Network_Appliction_Adress_Book.service.CustomUserDetailsService;

@Controller
public class HomePageController {
	@Autowired
	private AddressService addressService;

	@Autowired
	private AddressRepository FAddressRepository;

	@Autowired
	private UserRepository FUserRepository;	
	
    @Autowired
    JobLauncher jobLauncher;
 
    @Autowired
    Job exportAddressJob;
 

	@GetMapping("/home")
	public String home(Model model, @RequestParam(defaultValue = "0") int page) {
		model.addAttribute("Address", addressService.GetAddressListforUser());
		model.addAttribute("currentPage", page);		
		model.addAttribute("UserNum", FUserRepository.count());
		model.addAttribute("ContactNUm", FAddressRepository.count());
		return "userhome";
	}

/*	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") int id, Model model) {
		addressService.delete(id);
		return "redirect:/home";
	}
	*/
	
	// delete with contentNo
	@RequestMapping(value ="/delete/{id}", produces={"application/xml", "application/json"})
    @ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Map<String, Boolean> delete(@PathVariable("id") int id) {
		Map<String, Boolean> response = new HashMap<>();
		if (addressService.delete(id)) {			
			response.put("deleted", Boolean.TRUE);				
		}
		else {
			response.put("deleted", Boolean.FALSE);
		}
		return response;
	    }

	@RequestMapping(value= "/delete/{id}")
	public String delete(@PathVariable(value = "id") int addressId,Model model) throws Exception {
		delete(addressId);		
		return "redirect:/home";
	}


	// to get Update record for home page with contentNo
	@RequestMapping(value ="/edit/{id}", produces={"application/xml", "application/json"})
    @ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Address getUserId(@PathVariable("id") int id) {
		Address address = addressService.get(id);
		return address;
	    }
	

	@RequestMapping("/edit/{id}")
	public String Edit(@PathVariable("id") int id, Model model, @AuthenticationPrincipal UserDetails user) {
		Address address = addressService.get(id);	
		if (!address.getUser().getEmail().equals(user.getUsername()) & user.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals("ROLE_USER"))) 			
			return "redirect:/home";		
		model.addAttribute("Address", getUserId(id));
		return "EditAddress";
	}
	

	
	// for Update Button	
	@PostMapping("/update/{id}")
	public String updateAddress(@PathVariable("id") int id, @Valid @ModelAttribute("Address") Address address,
			BindingResult result) {
		if (result.hasErrors()) {
			return "EditAddress";
		}
		addressService.update(id, address);
		return "redirect:/home";
	}

	/*
	 * @PostMapping("/AddAddress") public String saveMessage(@Valid User user,
	 * BindingResult result, Model model) { FAddressRepository.save(address); return
	 * "redirect:/home"; }
	 */
	@GetMapping("/AddNewAddress")
	public String AddNewAddress(Model model) {
		model.addAttribute("Address", new Address());
		return "AddNewAdress";
	}
	
	// add wit content Nog
	@RequestMapping(value ="/AddAddress", produces={"application/xml", "application/json"})
    @ResponseStatus(code = HttpStatus.OK)
	public @ResponseBody Map<String, Boolean> add(@Valid @ModelAttribute("Address") Address address, BindingResult result) {
		Map<String, Boolean> response = new HashMap<>();
		if (addressService.add(address)) {			
			response.put("Add", Boolean.TRUE);				
		}
		else {
			response.put("Add", Boolean.FALSE);
		}
		return response;
	    }

	@RequestMapping("/AddAddress")
	public String AddAddress(@Valid @ModelAttribute("Address") Address address, BindingResult result) {
		if (result.hasErrors()) {
			return "AddNewAdress";
		}
		this.add(address, result);
		return "redirect:/home";
	}

	@GetMapping("/advanced")
	public String addressByBranch(Model model) {
		model.addAttribute("Address", addressService.getByUserBranch());
		return "userhome";
	}
	
	/*@RequestMapping(value = "/files/{fileName}", method = RequestMethod.GET)
	public HttpEntity<byte[]> createPdf(
	                 @PathVariable("fileName") String fileName) throws IOException {

		 String path = "src/main/resources/Address.csv";

	    HttpHeaders header = new HttpHeaders();
	    header.setContentType(MediaType.APPLICATION_PDF);
	    header.set(HttpHeaders.CONTENT_DISPOSITION,
	                   "attachment; filename=" + fileName.replace(" ", "_"));
	    header.setContentLength(path.length());

	    return new HttpEntity<byte[]>(
	                                  header);
	}*/

	
    @RequestMapping("/invokejob")
    public String handle() throws Exception {
 
            JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                    .toJobParameters();
            jobLauncher.run(exportAddressJob, jobParameters);
            
        return "redirect:/home";
    }
	
    @GetMapping("/marge/{DesID}/{SrcID}/{option}")
	public String Mearg(  	       
			@PathVariable("DesID") int DesID,@PathVariable("SrcID") int SrcID,
			@PathVariable("option") int option, Model model) {
		
		addressService.Merge(DesID,SrcID,option);
		
        return "redirect:/home";
		
		}	

}
