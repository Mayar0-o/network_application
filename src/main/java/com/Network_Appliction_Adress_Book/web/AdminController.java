package com.Network_Appliction_Adress_Book.web;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.Network_Appliction_Adress_Book.service.*;

@Controller
@PreAuthorize("hasRole('ADMIN')")
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private AddressService addressService;

	@Autowired
	private CustomUserDetailsService userService;

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/home")
	public String home(Model model) {
		model.addAttribute("Address", addressService.getAll());
		model.addAttribute("UserNum", userRepository.count());
		return "adminhome";
	}

	/*
	 * @GetMapping("/AddNewAddress") public String AddNewAddress(Model model) {
	 * model.addAttribute("Address", new Address()); return "AddNewAdress"; }
	 */

	@GetMapping("/pendingContacts")
	public String PendingContacts(Model model) {
		model.addAttribute("Address", addressService.GetAddressByValid(false));
		return "pendingContacts";
	}

	@GetMapping("/accept/{id}")
	public String updateUser(@PathVariable("id") int id) {
		addressService.SetValid(id);
		return "redirect:/admin/pendingContacts";
	}

	// TODO Add User controller
	@GetMapping("/users")
	public String UsersHome(Model model) {
		model.addAttribute("Users", userRepository.findAll());
		return "Users";
	}

	@GetMapping("/users/delete/{id}")
	public String DeleteUser(@PathVariable("id") int id) {
		userService.delete(id);
		return "redirect:/admin/users";
	}

	@GetMapping("/users/edit/{id}")
	public String EditUser(@PathVariable("id") int id, Model model) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid User Id:" + id));
		model.addAttribute("user", user);
		return "EditUser";
	}

	// for Update Button
	@PostMapping("/users/update/{id}")
	public String UpdateUser(@PathVariable("id") int id, @Valid @ModelAttribute("user") User user,
			BindingResult result) {
		if (result.hasErrors()) {
			return "EditUser";
		}
		userService.update(id, user);
		return "redirect:/admin/users";
	}
	

}
