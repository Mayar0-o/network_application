package com.Network_Appliction_Adress_Book.type;

public enum RoleEnum {

	ROLE_USER("ROLE_USER"), ROLE_ADMIN("ROLE_ADMIN"), ROLE_ADVANCED("ROLE_ADVANCED");

	private String name;

	RoleEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}