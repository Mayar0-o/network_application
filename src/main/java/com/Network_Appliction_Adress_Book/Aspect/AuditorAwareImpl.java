package com.Network_Appliction_Adress_Book.Aspect;
import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		
		SecurityContext securityContext = SecurityContextHolder.getContext();
		org.springframework.security.core.Authentication auth = securityContext.getAuthentication();
		
		return Optional.of(auth.getName());
	}

}