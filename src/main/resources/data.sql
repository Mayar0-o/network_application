
create table if not exists persistent_logins ( 
  username varchar(100) not null, 
  series varchar(64) primary key, 
  token varchar(64) not null, 
  last_used timestamp not null
);

delete from  user_role;
delete from  roles;
delete from  branch;
delete from  users;
delete from  contact;



INSERT INTO roles (id, name) VALUES 
(1, 'ROLE_ADMIN'),
(2, 'ROLE_ADVANCED'),
(3, 'ROLE_USER')
;
INSERT INTO branch(id, name) VALUES
(1,'Damascus');

INSERT INTO users (id, email, password, name, branch_id) VALUES 
(1, 'admin@gmail.com', '$2a$10$hKDVYxLefVHV/vtuPhWD3OigtRyOykRLDdUAp80Z1crSoS1lFqaFS', 'Admin', 1),
(2, 'user@gmail.com', '$2a$10$hKDVYxLefVHV/vtuPhWD3OigtRyOykRLDdUAp80Z1crSoS1lFqaFS', 'User', 1),
(3, 'user1@gmail.com', '$2a$10$hKDVYxLefVHV/vtuPhWD3OigtRyOykRLDdUAp80Z1crSoS1lFqaFS', 'User', 1);
;


insert into user_role(user_id, role_id) values
(1,1),
(2,3),
(3,3)
;

insert into contact(id, address, firstname, lastname, age, phone1,phone2, valid, user_id, version) 
VALUES(1,'Da','Miyar', 'Nour', 28, 9945651,56566831, 1, 2,0);
