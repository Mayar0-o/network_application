package com.Network_Appliction_Adress_Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.security.AuthenticatedUser;
import com.Network_Appliction_Adress_Book.service.CustomUserDetailsService;

@TestConfiguration
public class SpringSecurityTestConfig {

	
    @Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
    
    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
       
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));        
        org.springframework.security.core.userdetails.User user =  new org.springframework.security.core.userdetails.User("mh@gmail.com", passwordEncoder().encode("miyar"), authorities);        

        return new InMemoryUserDetailsManager(Arrays.asList(
                user
        ));
    }
    


}