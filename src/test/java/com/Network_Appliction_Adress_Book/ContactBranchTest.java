package com.Network_Appliction_Adress_Book;


import static org.junit.Assert.assertTrue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.http.*;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;


import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.junit.Test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.Network_Appliction_Adress_Book.NetworkApplictionAdressBookApplication;
import com.Network_Appliction_Adress_Book.entities.Address;
import com.Network_Appliction_Adress_Book.entities.Branch;
import com.Network_Appliction_Adress_Book.entities.User;
import com.Network_Appliction_Adress_Book.repositories.AddressRepository;
import com.Network_Appliction_Adress_Book.repositories.BranchRepository;
import com.Network_Appliction_Adress_Book.repositories.UserRepository;
import com.Network_Appliction_Adress_Book.service.AddressService;
import com.Network_Appliction_Adress_Book.web.AdminController;


import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
//@WebMvcTest(AdminController.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = SpringSecurityTestConfig.class)
public class ContactBranchTest {

	@Autowired
	private AddressService addressService;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private TestRestTemplate restTemplate;

	HttpHeaders headers = new HttpHeaders();

	@LocalServerPort
	int randomServerPort;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private UserRepository userRepository;

	private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ContactBranchTest.class);

//  @Sql({ "data.sql" })

//  init data by  data.sql file that have 3 users 
// user1 and user with Role_User at the same Branch
// user1 have address(Contact) that we Test access by user1    

	@Before
	public void init() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity()) // enable security for the mock set up
				.build();
	}

	

	@Test
	@WithMockUser(username = "user@gmail.com", roles = "USER")
	public void testGetContactSameBranch() throws Throwable {
		mvc.perform(get("/api/v1/address/1.json")).andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1))
				.andExpect(jsonPath("$.firstname").value("Miyar"))
				.andExpect(jsonPath("$.lastname").value("Nour"))
				.andExpect(jsonPath("$.age").value(28))
				.andExpect(jsonPath("$.address").value("Da"))
				.andExpect(jsonPath("$.valid").value(true))
				.andExpect(jsonPath("$.phone1").value(9945651))
				.andExpect(jsonPath("$.phone2").value(56566831));
	}
	
	

	@Test
	@Transactional
	@WithMockUser(username = "user@gmail.com", roles = "USER")
	public void UpdateContactSameBranch() throws Throwable {
		
		MockHttpServletRequestBuilder builder =
	              MockMvcRequestBuilders.put("/api/v1/update/address/1.json")
	                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
	                                    .accept(MediaType.APPLICATION_JSON)	                                    
	                                    .content("{\"address\":\"ss\"}");
		
	    this.mvc.perform(
	    		put("/api/v1/update/address/1.json")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON)                
                .content("{\"address\":\"ss\"}"
                		))
        .andDo(print()).andExpect(status().isOk());
	}
                

				

	@Test
	@Transactional
	@WithMockUser(username = "user1@gmail.com", roles = "USER")
	public void DeleteContact() throws Throwable {
		this.mvc.perform(MockMvcRequestBuilders
				.delete("/delete/1.json")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))	
				.andDo(print())
				.andExpect(jsonPath("$.deleted").value(false));
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + randomServerPort + uri;
	}

	@Test
	@WithMockUser(username = "admin@gmail.com", roles = "ADMIN")
	public void testAccessAdminRoles() throws Throwable{
		mvc.perform(get("/admin/home")).andExpect(status().isOk());
	}
	
	@Test
	
	@WithMockUser(username = "user@gmail.com", roles = "USER")
	public void test() {
		org.junit.Assert.assertTrue(true);
	}

	@Transactional
	@Test
	@WithMockUser(username = "user@gmail.com", roles = "USER")
	public void test1() {
		//Address address = new Address(4,"My","sad",21,"Damas");

		// LOGGER.info("info .............................");

		// addressRepository.save(address);
		// addressService.add(address);
		assertTrue(addressService.delete(1));
		//assertThat(addressRepository.findById(4)).isInstanceOf(Optional.class);
		// assertTrue(addressService.delete(1));
		// assertTrue(addressService.delete(1));
	}

}
