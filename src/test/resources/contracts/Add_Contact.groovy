import org.springframework.cloud.contract.spec.Contract
Contract.make {
  description("Add contact")
  request {
    method 'Post'
    url 'api/v1/address/add'
    body("""
  {
    "id":1,
    "firstname":"Miyar",
    "lastname":"Nour",
    "age":28,
    "address":"Da",
    "phone1":9945651,
    "phone2":56566831
  }
  """)
  }
 response {
    status 200
    headers {
      contentType(applicationJson())
    }
    body("""
  {
    "valid":true
  }
    """)
  
  }
}